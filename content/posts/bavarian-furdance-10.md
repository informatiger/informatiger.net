---
title: "Bavarian Furdance 10"
date: 2019-11-21T10:31:37+01:00
summary: "It's time to say again \"Welcome to the Bavarian Furdance\""
description: "Anniversary of a furdance"
tags: [Furry, Furdance]
type: article
draft: false
---

## The Bavarian Furdance 10

**A** few more days and then the long wait finally comes to an end.  
Then it's time to say again "Welcome to the Bavarian Furdance".  
This year in the 10th Anniversary Edition.  
Well, if that's not a reason to celebrate.  

I'm looking forward to it anyway and hope to see many familiar and new faces.

\- Icy

PS: There are still some slots left. You can register yourself at <https://bayern-furs.de/events/bfd/bfd10>.