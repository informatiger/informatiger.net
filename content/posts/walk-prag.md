---
title: "My first Fursuit Walk in Prague"
date: 2019-10-11T21:00:00+02:00
lastmod: 2019-10-31T20:00:00+02:00
summary: "Getting ready for a fursuit walk in Prague"
description: "Fursome adventures outside of Germany"
tags: [Furry, Suitwalk]
type: article
draft: false
---

#### Hello world, good news!

**A** good friend asked me if I want to join him at the fursuit walk in Prague.  
Well it’s not just an ordinary walk, it’s the Halloween fursuit walk (https://www.halloweenouting.cz/).

This will be my first suitwalk outside of Germany, and I am already really hyped for it.  
Next month I will travel via train to the friend, and then we’ll drive via car together to the walk on the next day.

I’m thinking of taking Sólas or the suit of my mate with me, but I’m not sure yet.  
However, I got told that I have still enough time to decide, so I won't hesitate with that.

I will post my choice after deciding.
  
<hr />
  
**Edit**: I will attend the suitwalk with the suit of Sólas:  
<img src="https://pbs.twimg.com/media/DnP0ELOXsAA-5wf.jpg" alt="Sólas" style="width:512px;"/>