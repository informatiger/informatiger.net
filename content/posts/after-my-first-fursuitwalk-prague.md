---
title: "After My First Fursuitwalk in Prague"
slug: "after-my-first-fursuitwalk-prague"
date: 2019-11-07T16:21:45+01:00
summary: "A little summary of the walk in Prague which happened at the start of november"
description: "Fursome adventures outside of Germany"
tags: [Furry, Suitwalk]
type: article
draft: false
---

## Every walk has to end,
**B**ut what remains are photos and beautiful memories.  
You find new friends and have a lot of fun.  
At least that is what I remember after a suitwalk.  

I had a lot of fun in prague wearing Sólas  
and putting a smile on the face of others, walking down the road.  

Looking forward to join this walk again next year.

-- Icy

---

<h2 class="has-text-centered" >Some photos shot by <a href="https://skollwolf.de">Sköll</a></h2>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<link href="https://unpkg.com/nanogallery2/dist/css/nanogallery2.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="https://unpkg.com/nanogallery2/dist/jquery.nanogallery2.min.js"></script>

<div id="ngy2p" data-nanogallery2='{
    "itemsBaseURL": "/images/fursuit-halloween-outing-prague-2019/",
    "thumbnailWidth": "512",
    "thumbnailBorderVertical": 0,
    "thumbnailBorderHorizontal": 0,
    "colorScheme": {
      "thumbnail": {
        "background": "rgba(68,68,68,0)"
      }
    },
    "thumbnailLabel": {
      "position": "onBottom"
    },
    "allowHTMLinData": true,
    "thumbnailAlignment": "center"
  }'>
  
  <a href="IMG_20191102_133335.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_133335.png"></a>
  <a href="IMG_20191102_133340.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_133340.jpg"></a>
  <a href="IMG_20191102_142729.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_142729.png"></a>
  <a href="IMG_20191102_150755.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_150755.jpg"></a>
  <a href="IMG_20191102_150901.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_150901.png"></a>
  <a href="IMG_20191102_140635.jpg" data-ngdesc="" data-ngthumb="thumbs/IMG_20191102_140635.jpg"></a>
  <a href="MVIMG_20191102_133326.jpg" data-ngdesc="" data-ngthumb="thumbs/MVIMG_20191102_133326.png"></a>
  <a href="MVIMG_20191102_144753.jpg" data-ngdesc="" data-ngthumb="thumbs/MVIMG_20191102_144753.jpg"></a>
</div>

<p style="text-align: center; font-size: x-large;">
  More pictures are available at <b><a href="https://link.skollwolf.de/hoprague2019">https://link.skollwolf.de/hoprague2019</a></b>
</p>