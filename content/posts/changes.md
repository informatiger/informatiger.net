---
title: "What's going on right now"
slug: "whats-going-on-right-now"
date: 2021-07-20T23:20:18+02:00
summary: "2021, what has changed? what has remained the same? where are we now?"
description: "A little update in 2021"
tags: [Update]
type: article
---

## The Corona pandemic has been going on for over a year now

What has changed? what has remained the same? where are we now?

### Corona and the floods

Let me make it short, even after a year, some individuals still think the pandemic is a government conspiracy. Vaccines expire due to missed appointments. The delta variant is currently spreading and the current vaccines have a weaker effect against this form.  

Meanwhile, parts of Germany and other countries are currently completely or partially under water. And of course, conspiracies like HAARP will come to the attention again and corona deniers and right-wingers are trying to profile from the situation, but not only them, also politicans like the chancellor candidate of the CDU or some politicians from the CDU. It is sad to see the (verbal) attacks agains rescue workers and (volunteer) helpers. As well as, of course, the many injured and dead people.  

We are facing a moment where we should see, that we are not prepaired against such things, whether because warnings are ignored or politicians do not (want to) see the problems.

### My life

In my life some things have changed, mostly to the good. I got a new job as a software engineer and am currently with my partner in the middle of a move, finally from the small 26 m² apartment in our new 3 room 82 m² home. The first relocation work is already done, but there is still a lot of work to do. 

-- Icy
