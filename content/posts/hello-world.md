---
title: "Hello World"
date: 2019-09-06T22:13:03+02:00
lastmod: 2020-07-22T10:00:00+02:00
summary: "Hi, my name is IceTiger and I am a german software developer"
description: "About myself"
tags: [Misc]
type: article
draft: false
---

### Hi,
#### my name is IceTiger and I am currently 21 years old.

- Software developer
- Started my training in 2017 at a German company
- Little backstory in C# on small private projects
- Work mostly with [C#](https://docs.microsoft.com/de-de/dotnet/csharp/) and [Kotlin](https://kotlinlang.org)

My current projects and some other stuff is located on [Gitlab](https://git.informatiger.net).
